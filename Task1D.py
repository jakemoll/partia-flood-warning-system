from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.utils import strip_attribute

def run():
    stations = build_station_list()
    rivers = rivers_with_station(stations)
    print(len(rivers))
    #print(sorted(list(rivers)))
    
    stat_by_riv = stations_by_river(stations)
    #print(rivers)
    #print(stat_by_riv)
    
    
    for river in ["River Aire", "River Cam", "River Thames"]:
        print(sorted(strip_attribute(stat_by_riv[river], "name")))

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()