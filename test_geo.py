from floodsystem.station import MonitoringStation
from floodsystem.geo import stations_by_distance
from floodsystem.geo import get_coordinates
from floodsystem.geo import get_distance
import floodsystem.geo
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list
import numpy as np

"""Unit test for the geo module"""

def fakeStation(i):
    """This function returns a fake station, given a seed i (i integer >= 0)"""
    station_id, measure_id = "stations.com/{}".format(i), i # set variables to stations.com/[i], and [i]
    noise = 1 + 0.005*i*(-1)**i # programatically generates noise for variation of river height
    coord = (i, i) # fake coordinates
    typical_range = (0.2*noise, 0.8*noise) # fake range
    river = "river {}".format(i)
    town = "town {}".format(i)
    label = "label {}".format(i)
    return MonitoringStation(station_id, measure_id, label, coord, typical_range, river, town)

def test_get_coordinates():
    stations = [fakeStation(i) for i in range(10)]
    assert np.all(np.isclose(get_coordinates(stations), [[i, i] for i in range(10)]))

def test_get_distance():
    stations = [fakeStation(i) for i in range(0, 10)]
    distances = get_distance(stations, r=0.5)
    assert distances[0] == 0.0
    for i in range(len(stations)):
        a = i * np.pi/180
        assert np.isclose(np.sin(distances[i]), np.sin(a/2)*np.sqrt(1 + np.cos(a)))

def test_stations_by_distance():
    stations = [fakeStation(i) for i in range(0, 20)]
    shuffled = stations
    np.random.shuffle(shuffled)
    assert stations_by_distance(stations) == stations_by_distance(shuffled)

def test_stations_within_radius():
    stations = [fakeStation(i) for i in range(0, 20)]
    by_distance = floodsystem.geo.stations_within_radius(stations, centre=(0,0), r=1000)
    assert get_distance([by_distance[-1]], (0, 0))[0] < 1000

def test_rivers_by_station_number():
    stations = build_station_list()
    river_sorted_stations = rivers_by_station_number(stations, 10)
    for i in range(len(river_sorted_stations)-1):
        assert river_sorted_stations[i][1] >= river_sorted_stations[i+1][1]