# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains utility functions.

"""


def sorted_by_key(x, i, reverse=False):
    """For a list of lists/tuples, return list sorted by the ith
    component of the list/tuple, E.g.

    Sort on first entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 0)
      >>> [(1, 2), (5, 1)]

    Sort on second entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 1)
      >>> [(5, 1), (1, 2)]

    """

    # Sort by distance
    def key(element):
        return element[i]

    return sorted(x, key=key, reverse=reverse)


def strip_attribute(MonitoringStations, attribute):
  """Returns just the specified attribute 
  (from station_id, measure_id, label, coord, typical_range, river, town)
  of the stations passed as a list of MonitoringStation objects"""
  return [getattr(station, attribute) for station in MonitoringStations]

def join_attribute(MonitoringStations, attribute):
  """This function returns a list of tuples (station, attribute)"""
  return list(zip(MonitoringStations, strip_attribute(MonitoringStations, attribute)))

def sort_by_attribute(MonitoringStations, attribute, reverse=False):
  """This function sorts a list of MonitoringStation objects by a given attribute
  from station_id, measure_id, label, coord, typical_range, river, town and returns the list
  sorted by that attribute"""
  stations = join_attribute(MonitoringStations, attribute)
  stations = sorted_by_key(stations, 1, reverse)
  return strip_zip(stations, 0)

def strip_zip(sequence, j):
  """Unzips a sequence of iterables and returns a list of the jth element of each
  iterable"""
  return [sequence[i][j] for i in range(len(sequence))]
