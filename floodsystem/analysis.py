import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates
import datetime

def polyfit(dates, levels, p):
    """Returns a numpy.poly1d object and a shift"""
    dates_shift = mpldates.date2num(dates)
    shift = dates_shift[-1] # set to be the last value so that if the dates go back a long time, the accuracy will be greatest at present time
    dates_shift = dates_shift - shift
    p_coeff = np.polyfit(dates_shift, levels, p)
    poly = np.poly1d(p_coeff)
    return(poly, shift)