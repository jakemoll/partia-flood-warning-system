from .utils import sort_by_attribute

def stations_level_over_threshold(stations, tol):
    """Returns list of tuples where each tuples holds:
    (i) a station object at which the latest relative water level is over the tolerance (tol)
    (ii) the relative water level at that station"""
    threshold_stations = []
    for station in stations:
        if ((station.typical_range_consistent()) and station.relative_water_level()!= None):
            if station.relative_water_level() > tol:
                threshold_stations.append((station, station.relative_water_level()))
    
    return threshold_stations

def stations_highest_rel_level(stations, N):
    """Returns a list of N station objects at which the relative water level is the highest
    List is sorted in descending order by relative water level"""
    station_list = [station for station in stations if station.relative_water_level() != None]
    station_sorted = sorted(station_list, key=lambda x: x.relative_water_level(), reverse=True)
    return station_sorted[:N]

