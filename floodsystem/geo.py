# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from .utils import sorted_by_key  # noqa
from .utils import strip_attribute
from .utils import strip_zip
from .utils import join_attribute
from .utils import sorted_by_key
import numpy as np

def get_coordinates(stations):
    """ This function takes an iterable of MonitoringStation objects and returns a numpy array of the
    coordinates of each station"""
    return np.array(strip_attribute(stations, "coord"))

def get_distance(stations, point=(0.0, 0.0), r=6371.0):
    """This function returns a list of distances between stations and the specifed coordinate,
    where the stations are passed as a list of MonitoringStations and point is a tuple of degree coordinates, 
    and returns a list of distances as floats in km; r in raidus of Earth in km"""
    # This function has been vectorised for efficiency, making it slightly more complicated :)
    # implementing the Haversine formula here, see https://en.wikipedia.org/wiki/Haversine_formula#Formulation
    lat1, long1 = [angle*np.pi/180 for angle in point] # converts the point to radians and splits its lat and longitude
    lat2, long2 = get_coordinates(stations).transpose()*np.pi/180 # generate a numpy vector of latitudes and longitudes of stations
    return list(2*r*np.arcsin(np.sqrt(np.sin(0.5*(lat2-lat1))**2 + np.cos(lat1)*np.cos(lat2)*np.sin(0.5*(long2-long1))**2))) # return a list of distances

def stations_by_distance(stations, p=(0.0, 0.0)):
    """This function returns a list of (station, distance) tuples, sorted by distance, which
    is the float distance of the monitoring station from the point p.
    Stations is a list of MonitoringStation objects and p is a tuple of floats for the coordinate p"""
    return sorted_by_key(zip(stations, get_distance(stations, p)), 1)

def stations_within_radius(stations, centre=(52.2053, 0.1218), r=10):
    """Returns a list of stations as MonitoringStation objects within a radius of r km (10km default)
    from the specified centre (default central Cambridge)"""
    stationlist = stations_by_distance(stations, centre)
    for i in range(len(stationlist)):
        #print(distances[i])
        if stationlist[i][1] > r:
            outside_index = i # outside_index stores the index of the first station to fall outside the radius specified
            print(i)
            break
    else:
        outside_index = None # include all stations
    
    return strip_zip(stationlist, 0)[:outside_index]


def rivers_with_station(stations):
    """This function returns a set of all the rivers which appear as the "river" attribute for a MonitoringStation in the list of MonitoringStations passed"""
    return set(strip_attribute(stations, "river"))

def stations_by_river(stations):
    """Returns a dictionary of keys: rivers, entries: stations on that river (as MonitoringStation objects)"""
    riverdict = dict(list(zip(list(rivers_with_station(stations)), [[] for i in range(len(stations))])))
    for station in stations:
        riverdict[station.river].append(station)
        #print(station.river)
    return riverdict

def rivers_by_station_number(stations, N):
    """Returns a list of (river name, number of stations) tuples from a list of MonitoringStation objects (stations)
    The top N stations are returned, plus any below the Nth with the same number of stations"""
    rivers_sorted = []
    river_list = stations_by_river(stations)
    for key, entry in river_list.items():
        rivers_sorted.append((key, len(entry)))
    rivers_sorted = sorted_by_key(rivers_sorted, 1, True)
    print_list = rivers_sorted[:N]
    low_num = rivers_sorted[N-1][1]
    for i in range (N, len(rivers_sorted)):
        if rivers_sorted[i][1] == low_num:
            print_list.append(rivers_sorted[i])
    return print_list
    