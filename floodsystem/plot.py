import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, timedelta
from .analysis import polyfit
import matplotlib.dates as mpldates

def plot_water_levels(station, dates, levels):
    """Plot the water levels for a given MonitoringStation object for a list of dates and a corresponding list of water levels"""
        # Plot
    plt.plot(dates, levels)
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('Date')
    plt.ylabel('Water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    """Plot the water levels of a station with the best-fit polynomial of the specified degree p"""
    plt.plot(dates, levels)
    poly, shift = polyfit(dates, levels, p)
    plt.plot(dates, np.polyval(poly, mpldates.date2num(dates)-shift))
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('Date')
    plt.ylabel('Water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
