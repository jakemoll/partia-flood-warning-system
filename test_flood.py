"""Unit test for the flood module"""
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level
from floodsystem.station import MonitoringStation

def test_stations_level_over_threshold():
    """Test the stations_level_over_threshold function"""
    # Create 2 monitoring stations, one with relative water level above threshold and another below threshold
    station_over = MonitoringStation(None, None, "Station over threshold", None, (0, 1), None, None)
    station_over.latest_level = 0.81
    station_under = MonitoringStation(None, None, "Station under threshold", None, (0, 1), None, None)
    station_under.latest_level = 0.79
    # Create list of these two stations
    stations_test_list = [station_over, station_under]
    returned_stations = stations_level_over_threshold(stations_test_list, 0.8)
    
    assert len(returned_stations) == 1
    assert returned_stations[0][0] == station_over
    
def test_stations_highest_rel_level():
    """Test the stations_highest_rel_level funtion"""
    # Create series of monitoring stations for test
    station_none_typical = MonitoringStation(None, None, "Station over threshold", None, None, None, None) # station with typical range as None
    station_none_typical.latest_level = 0.6
    station_none_latest = MonitoringStation(None, None, "Station over threshold", None, (0, 1), None, None) # station with latest level as None
    station_none_latest.latest_level = None
    station_1 = MonitoringStation(None, None, "Station over threshold", None, (0, 1), None, None) # station with acceptable data with higher relative water level
    station_1.latest_level = 0.81
    station_2= MonitoringStation(None, None, "Station under threshold", None, (0, 1), None, None) # station with acceptable data with lower relative water level
    station_2.latest_level = 0.79

    stations_test_list = [station_none_typical, station_none_latest, station_1, station_2]
    # Return list of length 1
    returned_list_one = stations_highest_rel_level(stations_test_list, 1)
    # Return list of length 2
    returned_list_two = stations_highest_rel_level(stations_test_list, 2)
    assert len(returned_list_one) == 1
    assert returned_list_one == [station_1]
    assert len(returned_list_two) == 2
    assert returned_list_two == [station_1, station_2]

