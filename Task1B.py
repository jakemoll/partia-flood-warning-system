from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def station_simple_id(MonitoringStation, distance):
    return(MonitoringStation.name, MonitoringStation.town, distance)


def run():
    """Requirements for Task 1B"""
    stations = [station_simple_id(entry[0], entry[1]) for entry in stations_by_distance(build_station_list(), (52.2053, 0.1218))]
    print(stations[:10])
    print(stations[-10:])

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()