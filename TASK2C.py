from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    stations = build_station_list(use_cache=False)
    update_water_levels(stations)
    highest_rel_level_stations = stations_highest_rel_level(stations, 10)
    for station in highest_rel_level_stations:
        print (station)
        print(station.name, station.relative_water_level())

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()