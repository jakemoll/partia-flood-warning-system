from floodsystem.plot import plot_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.analysis import polyfit
import numpy as np
import matplotlib.pyplot as plt
import datetime

def run():
    stations = build_station_list(use_cache=True) # CHANGE THIS TO FALSE WHEN DONE!!!!!!!!!!!
    update_water_levels(stations)
    dt = 7 # num days behind to draw from prediction
    degree = 4
    prediction_range = 5 # num of days ahead to predict
    threshold = 2
    crit_threshold = 5
    time = np.linspace(0, prediction_range, 1000)
    for station in stations:
        try:
            dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
            poly = polyfit(dates, levels, degree)[0]
            typ_range = station.typical_range
            predicted_relative_levels = (np.polyval(poly, time)-typ_range[0])/(typ_range[1] - typ_range[0])
            max_rel_level = np.max(predicted_relative_levels)
            if max_rel_level >= threshold:
                print("""Warning: Station {} predicted to have a relative water level of {} in the next {} days.\nCurrent information:\n{!r}""".format(station.name, round(max_rel_level, 2), prediction_range, station))
                if max_rel_level >= crit_threshold:
                    plot_water_levels(station, dates, levels)
        except:
            pass
        

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()