from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold

def run():
    stations = build_station_list(use_cache=False)
    update_water_levels(stations)
    stations_over_threshold = stations_level_over_threshold(stations, 0.8)
    for station in stations_over_threshold:
        print(station[0].name, station[1])

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
