from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list
from floodsystem.utils import strip_attribute
from floodsystem.utils import sort_by_attribute
def run():
    """Requirements for Task 1C"""

    stations = build_station_list()
    nearby = stations_within_radius(stations, r=10.0)
    #print(nearby)
    print(strip_attribute(sort_by_attribute(nearby, "name"), "name"))

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()