"""Unit test for the utils module"""

import floodsystem.utils
import test_geo
from test_geo import fakeStation
import numpy as np

def test_sort():
    """Test sort container by specific index"""

    a = (10, 3, 3)
    b = (5, 1, -1)
    c = (1, -3, 4)
    list0 = (a, b, c)

    # Test sort on 1st entry
    list1 = floodsystem.utils.sorted_by_key(list0, 0)
    assert list1[0] == c
    assert list1[1] == b
    assert list1[2] == a

    # Test sort on 2nd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 1)
    assert list1[0] == c
    assert list1[1] == b
    assert list1[2] == a

    # Test sort on 3rd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 2)
    assert list1[0] == b
    assert list1[1] == a
    assert list1[2] == c


def test_reverse_sort():
    """Test sort container by specific index (reverse)"""

    a = (10, 3, 3)
    b = (5, 1, -1)
    c = (1, -3, 4)
    list0 = (a, b, c)

    # Test sort on 1st entry
    list1 = floodsystem.utils.sorted_by_key(list0, 0, reverse=True)
    assert list1[0] == a
    assert list1[1] == b
    assert list1[2] == c

    # Test sort on 2nd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 1, reverse=True)
    assert list1[0] == a
    assert list1[1] == b
    assert list1[2] == c

    # Test sort on 3rd entry
    list1 = floodsystem.utils.sorted_by_key(list0, 2, reverse=True)
    assert list1[0] == c
    assert list1[1] == a
    assert list1[2] == b

def test_strip_attribute():
    stations = [test_geo.fakeStation(i) for i in range(0, 9)]
    assert floodsystem.utils.strip_attribute(stations, "measure_id") == list(range(0,9))

def test_sort_by_attribute():
    stations = [fakeStation(i) for i in range(0, 20)]
    shuffled = stations.copy()
    np.random.shuffle(shuffled)
    #assert stations == floodsystem.utils.sort_by_attribute(stations, "name")
    assert stations == floodsystem.utils.sort_by_attribute(stations, "measure_id")

def test_strip_zip():
    sequence = list(zip(list(range(0, 10, 2)),list(range(1, 11, 2))))
    assert [sequence[i][0] for i in range(len(sequence))] == list(range(0, 10, 2))

def test_join_attribute():
    stations = [fakeStation(i) for i in range(0, 20)]
    assert floodsystem.utils.strip_zip(floodsystem.utils.join_attribute(stations, "name"), 0) == stations
