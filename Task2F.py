from floodsystem.plot import plot_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.plot import plot_water_level_with_fit
import datetime


def run():
    stations = build_station_list(use_cache=True) # CHANGE THIS TO FALSE WHEN DONE!!!!!!!!!!!
    update_water_levels(stations)
    dt = 2
    num_stations = 5
    degree = 4
    for station in stations_highest_rel_level(stations, num_stations):
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        plot_water_level_with_fit(station, dates, levels, degree)

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()